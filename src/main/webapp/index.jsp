<html>
<body>
<h2>Hello World with OpenShift!</h2>
<%
    out.print("<p>Remote Addr: " + request.getRemoteAddr() + "</p>");
    out.print("<p>Remote Host: " + request.getRemoteHost() + "</p>");
    out.print("<p>X-Forwarded-For: " + request.getHeader("x-forwarded-for") + "</p>");
%>
</body>
</html>
